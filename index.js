const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://Atanpogi:admin123@batch204-nuestrojonatha.vsomfie.mongodb.net/B204-to-dos?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "connection error"));

db.once("open", () => console.log("Naka konekta na sa Base ng Data"));

app.use(express.json());

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`nakikinig sa port ${port}`));