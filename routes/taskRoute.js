const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController")

//Routes

//Routes to get all the task

router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))

});

router.post("/", (req, res) => {
	console.log(req.body)

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

router.delete("/:id", (req, res) => {
	console.log(req.params)

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id", (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

